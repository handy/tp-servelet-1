package dao;


import modele.Client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDAO {

    private static String URL = "jdbc:mysql://localhost:3306/Client";
    private static String USER = "userpop";
    private static String PW = "userpop";

    private static String SQLFindClientByAlias =
            "SELECT nom, prenom, email " +
                    "FROM Client " +
                    "WHERE alias = ? AND password = ?";

    private static String SQLInsertClient =
            "INSERT INTO Client VALUES(?, ?, ?, ?, ?)";

    private Connection conn = null;

    private static ClientDAO clientDao = null;

    public static ClientDAO getSingleton(){
        if(clientDao == null){
            clientDao = new ClientDAO();
        }
        return clientDao;
    }

    private ClientDAO(){
        try {
            conn = DriverManager.getConnection(URL, USER, PW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Client find(String alias, String password) throws Exception {

        if (alias == null || password == null) return null;
            Client client = null;
            PreparedStatement selectStatement = null;
            ResultSet resultSet = null;

        try {
            selectStatement = conn.prepareStatement(SQLFindClientByAlias);
            selectStatement.setString(1, alias.toLowerCase());
            selectStatement.setString(2, password);

            resultSet = selectStatement.executeQuery();

            if (resultSet.next()){
                String nom = resultSet.getString("nom");
                String prenom = resultSet.getString("prenom");
                String email = resultSet.getString("email");

                client = new Client(alias,password,nom,prenom,email);
            }

            return client;

        } finally {
            resultSet.close();
            selectStatement.close();
        }
    }

    public Client create(String alias, String password, String nom, String prenom, String email) throws Exception{

        if (alias == null || password == null || nom == null || prenom == null || email ==null) return null;
        Client client = null;
        PreparedStatement insertStatement = null;
        ResultSet resultSet = null;
        int response = 0;
        PreparedStatement selectStatement = null;

        try {

            selectStatement = conn.prepareStatement(SQLFindClientByAlias);
            selectStatement.setString(1, alias.toLowerCase());
            selectStatement.setString(2, password);

            resultSet = selectStatement.executeQuery();

            if (resultSet.next()) return null;

            insertStatement = conn.prepareStatement(SQLInsertClient);
            insertStatement.setString(1, alias.toLowerCase());
            insertStatement.setString(2, password);
            insertStatement.setString(3, nom);
            insertStatement.setString(4, prenom);
            insertStatement.setString(5, email);

            conn.setAutoCommit(false);
            response = insertStatement.executeUpdate();

            conn.commit();

            if (response > 0){
                client = new Client(alias, password, nom, prenom, email);
            }


        }catch (Exception e) {
            e.printStackTrace();
            conn.rollback();
        }
        return client;
    }

    public static void main(String[] args) throws Exception {
    }
}
