package modele;

import java.io.Serializable;
import java.util.Objects;

public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    private String alias;
    private String password;
    private String nom;
    private String prenom;
    private String email;

    public Client(String alias, String password, String nom, String prenom, String email) {
        setAlias(alias);
        setPassword(password);
        setNom(nom);
        setPrenom(prenom);
        setEmail(email);
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return getEmail().equals(client.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

    @Override
    public String toString() {
        return "Client{" +
                "alias='" + getAlias() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", nom='" + getNom() + '\'' +
                ", prenom='" + getPrenom() + '\'' +
                ", email='" + getEmail() + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Client client = new Client("bizarre", "1234", "NATHALIE", "GATOR", "nagator@yhoo.fr");
        System.out.println(client);
    }
}
