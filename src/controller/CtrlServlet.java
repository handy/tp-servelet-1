package controller;

import dao.ClientDAO;
import modele.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/process")
public class CtrlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            String action = request.getParameter("action");

            switch (action) {
                case "authenticate":
                    doAuthenticate(request, response);
                    break;
                case "login":
                    doLogin(request, response);
                    break;
                case "afficherClient":
                    doAfficherFormEnrClient(request, response);
                    break;
                case "enrClient":
                    doEnrClient(request, response);
                    break;
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void doLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "pages/login.jsp";
        forward(url, request, response);
    }

    protected void doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        try {

            String alias = request.getParameter("alias");
            String password = request.getParameter("password");

            Client client = ClientDAO.getSingleton().find(alias, password);
            if (client != null) {
                request.setAttribute("client", client);
            }

            String url = (client == null ? "pages/unknown.jsp" : "pages/known.jsp");
            forward(url, request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doAfficherFormEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "pages/register.jsp";
        forward(url, request, response);
    }

    protected void doEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String alias = request.getParameter("alias");
            String password = request.getParameter("password");
            String nom = request.getParameter("nom");
            String prenom = request.getParameter("prenom");
            String email = request.getParameter("email");

            Client client = ClientDAO.getSingleton().create(alias, password, nom, prenom, email);
            if (client != null){
                request.setAttribute("client", client);
            }
            String url = (client == null ? "pages/unknown.jsp" : "pages/known.jsp");
            forward(url, request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
